import React from "react";
import {
  Flex,
  Heading,
  Image,
  Text,
  Textarea,
  FormControl,
  Input,
  Button,
  Checkbox,
  FormLabel,
} from "@chakra-ui/core";

const ContactFooter = (props) => {
  const property = {
    map: require("./../../assets/images/Maps.jpg"),
  };
  return (
    <Flex
      padding="60px 5%"
      justifyContent="space-between"
      backgroundColor="secondary"
      width="100%"
      direction="column"
      className="row space-around"
    >
      <Flex direction="column" alignItems="center" className="lg-30">
        <Heading as="h3" marginBottom="30px">
          Plan d'accès
        </Heading>
        <Image src={property.map} />
      </Flex>
      <Flex direction="column" alignItems="center" className="lg-25">
        <Heading as="h3" marginBottom="30px">
          Informations Pratiques
        </Heading>
        <Flex
          padding="40px"
          backgroundColor="white"
          shadow="md"
          direction="column"
          alignItems="center"
        >
          <Heading as="h4">Adresse</Heading>
          <Text width="100%" textAlign="center">
            105 Rue de Maubeuge <br />
            75010 Paris
          </Text>
          <Heading as="h4">Moyens d'accès</Heading>
          <Text>
            Métro: arrêt Gare du Nord, lignes 4, 5<br />
            RER: B, D, E, H, K<br />
            Bus: 38, 39, 42, 43, 46, 302
            <br />
            Velib': station n°10028
          </Text>
          <Heading as="h4">Horaires d'ouverture</Heading>
          <Text>Ouvert tous les jours de 9h00 à 19h00. Sauf le dimanche.</Text>
        </Flex>
      </Flex>
      <Flex direction="column" alignItems="center" className="lg-30">
        <Heading as="h4" marginBottom="30px">
          formulaire de contact
        </Heading>
        <FormControl
          isRequired
          display="flex"
          flexDirection="column"
          justifyContent="space-around"
          width="100%"
          padding="40px"
          backgroundColor="white"
          shadow="md"
        >
          <FormLabel marginTop="10%">Email</FormLabel>
          <Input
            type="email"
            id="email"
            placeholder="Email"
            marginTop="5%"
            width="100%"
          />
          <FormLabel htmlFor="message" marginTop="10%">
            Votre message
          </FormLabel>
          <Textarea type="text" id="message" marginTop="5%" width="100%" />
          <Checkbox>
            J'accepte que mes informations personnelles soient utilisées pour me
            répondre
          </Checkbox>
          <Button
            height="35px"
            width="100px"
            border="3px solid"
            borderColor="primary"
            color="primary"
            backgroundColor="main"
            marginTop="15px"
            className="button"
            type="submit"
          >
            Valider
          </Button>
        </FormControl>
      </Flex>
    </Flex>
  );
};

export default ContactFooter;
