import React from "react";
import { Flex, Image, Box, Text, Heading, Button } from "@chakra-ui/core";
import ContactFooter from "./ContactFooter";
import { useHistory } from "react-router-dom";

const Home = (props) => {
  const property = {
    banner: require("./../../assets/images/shridhar-gupta-dZxQn4VEv2M-unsplash.jpg"),
    imageCoffee: require("./../../assets/images/asset1.jpg"),
    imageRoom: require("./../../assets/images/asset3.jpg"),
    logo: require("./../../assets/images/logo.png"),
  };
  const history = useHistory();
  return (
    <Flex direction="column" alignItems="center">
      <Flex>
        <Image src={property.banner} />
        <Image
          src={property.logo}
          position="absolute"
          top="5vh"
          left="41%"
          width="20%"
          className="top-15vh"
        />
      </Flex>

      <Flex
        direction="column"
        padding="5%"
        bgSize="contain"
        bgPos="left bottom"
        className="intro bg row space-around"
      >
        <Flex
          direction="column"
          alignItems="center"
          padding="40px"
          backgroundColor="white"
          shadow="md"
          className="lg-25"
        >
          <Heading as="h2" fontFamily="Nature Spirit" marginBottom="30px">
            Un Coffee Shop
          </Heading>
          <Text>
            Gusto Coffee c'est bien sûr un espace chaleureux. Vous pouvvez y
            déguster nos meilleurs cafés. Elaborées avec soin, nous avons une
            graande quantité de boissons chaudes et froides.
          </Text>
          <Button
            height="48px"
            width="200px"
            border="3px solid"
            borderColor="primary"
            color="primary"
            backgroundColor="main"
            marginTop="15px"
            className="button"
            onClick={() => {
              history.push("/menu");
            }}
          >
            Plus d'infos
          </Button>
        </Flex>
        <Box size="sm">
          <Image src={property.imageCoffee} alt="Café" />
        </Box>
      </Flex>
      <Flex
        justifyContent="space-around"
        alignItems="center"
        padding="5%"
        direction="column"
        className="bg-reverse row-reverse"
      >
        <Flex
          direction="column"
          alignItems="center"
          padding="40px"
          backgroundColor="white"
          boxShadow="0px 5px 10px main"
          shadow="md"
          className="lg-25 space-around"
        >
          <Heading as="h2" fontFamily="Nature Spirit" marginBottom="30px">
            Un Espace de Coworking
          </Heading>
          <Text>
            Pour vos rendez-vous profesionnels ou pour travailler dans un lieu
            confortable. Gusto Coffee met à votre disposition un espace de
            coworking ainsi que des salons privés.
          </Text>
          <Button
            height="48px"
            width="200px"
            border="3px solid"
            borderColor="primary"
            color="primary"
            backgroundColor="main"
            marginTop="15px"
            className="button"
            onClick={() => {
              history.push("/coworking");
            }}
          >
            Plus d'infos
          </Button>
        </Flex>
        <Box size="sm">
          <Image src={property.imageRoom} alt="Café" />
        </Box>
      </Flex>
      <ContactFooter />
    </Flex>
  );
};

export default Home;
