import React from "react";
import { Box, Image, Flex, Text, Link, Button, Heading, Icon } from "@chakra-ui/core";
import Carousel, { consts } from "react-elastic-carousel";
import { useHistory } from "react-router-dom";

const CoWorking = props => {
  const property = {
    banner: require("../../assets/images/coworking-banner.jpg"),
    salleImg: require("../../assets/images/salle.jpg"),
    background: "url('../../assets/images/background_accueil.png')",
    backgroundReverse:
      "url('../../assets/images/background_accueil-reverse.png')",
    text: {
      promo:
        "Ea nostrud nisi veniam cupidatat cillum commodo cupidatat ad ex culpa laborum deserunt. Laborum irure qui nulla pariatur excepteur ex veniam ipsum tempor nostrud aute sunt anim sunt. Ullamco culpa culpa amet voluptate mollit et do qui aliqua consequat nostrud incididunt sunt. Ea nulla pariatur est aute ullamco deserunt in qui nisi ullamco.",
      salle:
        "Officia ad Lorem culpa occaecat in nostrud irure. Minim adipisicing non aliqua quis in. Anim nostrud nulla officia aliquip enim minim velit labore minim. Qui ipsum proident consequat anim amet in exercitation fugiat ut enim et quis. Aliquip culpa ut id nisi.",
      salon:
        "Velit ad sint occaecat ullamco exercitation ex est irure excepteur aliquip do ex ex minim. Nisi minim dolor reprehenderit minim. Ea labore sunt veniam deserunt culpa est esse excepteur cillum. Consequat ex cupidatat ullamco aute duis cupidatat pariatur elit.",
    },
    salon4p: {
      1: require("../../assets/images/salon-4p-1.jpg"),
      2: require("../../assets/images/salon-4p-2.jpg"),
      3: require("../../assets/images/salon-4p-3.jpg"),
    },
    salon6p: {
      1: require("../../assets/images/salon-6p-1.jpg"),
      2: require("../../assets/images/salon-6p-2.jpg"),
      3: require("../../assets/images/salon-6p-3.jpg"),
    },
  };

  const history = useHistory();

  const myArrow = ({ type, onClick, isEdge }) => {
    const pointer = type === consts.PREV ? (
      <Icon name="chevron-left" size="75px" color="primary" />
    ) : (<Icon name="chevron-right" size="75px" color="primary" />);

    return <Button onClick={onClick} disabled={isEdge}>{pointer}</Button>
  };

  return (
    <Box paddingBottom="60px">
      <Box width="100vw">
        <Image src={property.banner} alt="Bannière Coworking" />
      </Box>
      <Flex direction="column" align="center" padding="0% 5%" bgSize="contain" bgPos="left bottom" className="intro bg">
        <Text className="block-text lg-60" marginBottom="45px">
          {property.text.promo}
        </Text>
        <Flex justify="space-around" width="100%" marginTop="10px">
          <Link href="#salle" _hover={{ textDecoration: "none", fontWeight: "Bold" }}>
            <Heading as="h3" size="lg" fontWeight="inherit">Salle Commune</Heading>
          </Link>
          <Link href="#salon" _hover={{ textDecoration: "none", fontWeight: "Bold" }}>
            <Heading as="h3" size="lg" fontWeight="inherit">
              Salons Privés
            </Heading>
          </Link>
        </Flex></Flex>
      <Flex direction="column" align="center" padding="5%" className="bg-reverse">
        <Heading as="h2" paddingBottom="10px" id="salle">Salle Commune</Heading>
        <Flex direction="column" className="row space-around align-center" margin="30px 0">
          <Text marginBottom="10px" padding="10px" shadow="md" className="lg-40 block-text">{property.text.salle}</Text>
          <Image src={property.salleImg} className="lg-40" />
        </Flex>
        <Button height="48px" width="200px" border="3px solid" borderColor="primary" color="primary" backgroundColor="main" marginTop="15px" className="button" onClick={() => history.push("/booking")}>Réserver une place</Button>
      </Flex>
      <Flex align="center" direction="column" padding="5%" className="bg">
        <Heading as="h2" paddingBottom="10px" id="salon">
          Salons Privés 4 Places
        </Heading>
        <Flex
          width="100%"
          direction="column"
          padding="15px 0"
          className="row-reverse space-around align-center"
          margin="30px 0"
        >
          <Text marginBottom="10px" padding="10px" shadow="md" className="lg-40 block-text"
          >
            {property.text.salon}
          </Text>
          <Carousel
            itemsToShow={1}
            renderArrow={myArrow}
            pagination={false}
            className="lg-40">
            <Image src={property.salon4p[1]} />
            <Image src={property.salon4p[2]} />
            <Image src={property.salon4p[3]} />
          </Carousel>
        </Flex>
        <Button height="48px" width="200px" border="3px solid" borderColor="primary" color="primary" backgroundColor="main" marginTop="15px" className="button" onClick={() => history.push("/private-room-booking")}>Réserver un salon</Button>
      </Flex>
      <Flex
        direction="column"
        align="center"
        padding="5%"
        className="bg-reverse"
      >
        <Heading as="h2" paddingBottom="10px">
          Salons Privés 6 Places
        </Heading>
        <Flex
          width="100%"
          direction="column"
          padding="15px 0"
          className="row space-around align-center"
        >
          <Text
            marginBottom="10px"
            padding="10px"
            shadow="md"
            className="lg-40 block-text"
          >
            {property.text.salon}
          </Text>
          <Carousel
            itemsToShow={1}
            renderArrow={myArrow}
            pagination={false}
            className="lg-40"
          >
            <Image src={property.salon6p[1]} />
            <Image src={property.salon6p[2]} />
            <Image src={property.salon6p[3]} />
          </Carousel>
        </Flex>
        <Button
          height="48px"
          width="200px"
          border="3px solid"
          borderRadius="5px"
          borderColor="primary"
          color="primary"
          backgroundColor="main"
          marginTop="10%"
          className="button"
          onClick={() => history.push("/private-room-booking")}
        >
          Réserver un salon
        </Button>
      </Flex>
    </Box >
  );
};

export default CoWorking;
