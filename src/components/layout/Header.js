import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.css";
import logo from "../../assets/images/gusto-coffee.png";
import { FaPowerOff, FaMapMarkedAlt, FaShoppingBasket } from "react-icons/fa";
import { Buymeacoffee } from "@icons-pack/react-simple-icons";
import { Flex, Box, Image, Avatar, Heading } from "@chakra-ui/core";
import { useAuth } from "../../services/AuthProvider";

export const Header = () => {
  let screen = window.innerWidth;
  const { user } = useAuth();

  if (screen >= 1024) {
    return (
      <Flex
        align="center"
        justify="space-around"
        backgroundColor="main"
        height="100px"
        className="header"
      >
        <Flex align="flex-end" width="40%" justify="space-around">
          <NavLink to="/menu">
            <Heading as="h2" size="lg">
              La Carte
            </Heading>
          </NavLink>
          <NavLink to="/coworking">
            <Heading as="h2" size="lg">
              Co-Working
            </Heading>
          </NavLink>
        </Flex>
        <Flex width="20%" align="center" justify="center">
          <NavLink to="/">
            <img className="logo" src={logo} alt="Logo" />
          </NavLink>
        </Flex>
        <Flex width="40%" justify="space-around">
          <NavLink to="/contact">
            <Heading as="h2" size="lg">
              Contact
            </Heading>
          </NavLink>
          <NavLink to="/cart">
            <Heading as="h2" size="lg">
              Panier
            </Heading>
          </NavLink>
          {user ? (
            <NavLink to="/user">
              <Avatar name="Dan Abrahmov" src="https://bit.ly/dan-abramov" />
            </NavLink>
          ) : (
            <NavLink to="/connexion">
              <FaPowerOff size="28px" />
            </NavLink>
          )}
        </Flex>
      </Flex>
    );
  } else {
    return (
      <Flex
        align="center"
        justify="space-around"
        backgroundColor="main"
        height="60px"
        className="header"
      >
        <Flex align="flex-end" width="40%" justify="space-around">
          <NavLink to="/menu">
            <Box as={Buymeacoffee} height="40px" width="auto" color="primary" />
          </NavLink>
          <NavLink to="/coworking">
            <Box
              as={FaMapMarkedAlt}
              height="40px"
              width="auto"
              color="primary"
            />
          </NavLink>
        </Flex>
        <Flex width="20%" align="center" justify="center">
          <NavLink to="/">
            <Image className="logo" src={logo} alt="Logo" height="40px" />
          </NavLink>
        </Flex>
        <Flex width="40%" justify="space-around">
          <NavLink to="/cart">
            <Box
              as={FaShoppingBasket}
              height="40px"
              width="auto"
              color="primary"
            />
          </NavLink>
          {user ? (
            <NavLink to="/user/profil">
              <Avatar
                name="Dan Abrahmov"
                src="https://bit.ly/dan-abramov"
                height="40px"
                width="40px"
              />
            </NavLink>
          ) : (
            <NavLink to="/connexion">
              <Box as={FaPowerOff} height="35px" width="auto" color="primary" />
            </NavLink>
          )}
        </Flex>
      </Flex>
    );
  }
};
