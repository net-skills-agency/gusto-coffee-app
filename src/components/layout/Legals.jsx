import { Flex, Heading, Text, List, ListItem } from "@chakra-ui/core";
import React from "react";

const Legals = (props) => {
  return (
    <Flex
      direction="column"
      className="mt-130"
      alignItems="center"
      padding="5% 10%"
    >
      <Heading as="h2" margin="30px 0">
        Mentions légales
      </Heading>
      <Text>
        En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la
        confiance dans l'économie numérique, il est précisé aux utilisateurs du
        site gusto-coffee.eu l'identité des différents intervenants dans le
        cadre de sa réalisation et de son suivi :
        <List styleType="disc">
          <ListItem>
            Propriétaire : Société Gusto-coffee – SARL 484 969 250 – 10 rue des
            lilas Créteil 94000, capital de 10 000€
          </ListItem>
          <ListItem>Créateur : Société NSA</ListItem>
          <ListItem>
            Responsable publication : Alain Belluci –
            a.bellucci@gusto-coffee561848.fr / 01 46 11 81 19. Le responsable de
            publication est une personne physique.
          </ListItem>
          <ListItem>
            Webmaster : Alain Belluci – a.bellucci@gusto-coffee561848.fr / 01 46
            11 81 19
          </ListItem>
          <ListItem>
            Hébergeur : OVH SAS – 2 rue Kellermann 59100 Roubaix France
          </ListItem>
        </List>
      </Text>
      <Heading as="h2" margin="30px 0">
        Politique de protection des données
      </Heading>
      <Text>
        La présente charte de protection des données personnelles et de gestion
        des cookies détaille la politique de la Société Gusto-coffee, en matière
        de protection de la vie privée, de collecte de données personnelles et
        de cookies.
        <br /> Elle s’applique aux données collectées sur le site de
        Gusto-coffee.
        <br /> La charte a pour but d’informer sur les moyens que mis en œuvre
        pour collecter les données à caractère personnel de ses utilisateurs,
        dans le respect le plus strict de ses droits, sur les données
        susceptibles d’être recueillies sur le site et sur l’usage qui peut être
        fait des données collectées et les droits des utilisateurs sur ces
        données.
        <br /> Gusto-coffee s’engage en retour à veiller au respect des
        principes sur la protection des données personnelles, informer les
        personnes au sujet de l’existence de leurs droits d’accès, de
        rectification et d’opposition, procéder à l’accomplissement des
        formalités légales et/ou réglementaires.
        <br />
      </Text>
      <Text>
        <Heading as="h3" fontSize="24px" fontWeight="500">
          Protection des données
        </Heading>
        Les données à caractère personnel, sont les données qui permettent
        d’identifier un utilisateur directement ou indirectement (par
        regroupement d’informations) tels que le nom, prénom, email, téléphone,
        diplôme, profession, etc ...
        <br /> Lors de l’utilisation du site Internet, et par l’intermédiaire de
        celui-ci, des données à caractère personnel seront susceptibles d’être
        collectées, lesquels seront enregistrés lors de :<br />
        <List styleType="disc">
          <ListItem>
            La création d’un compte client dans le but de commander/réserver
            dans notre coffee-shop coworking
          </ListItem>
          <ListItem>Le contact de notre service client</ListItem>
          <ListItem>
            La mise en relation de nos utilisateurs à leur demande sur le site
          </ListItem>
        </List>
        Conformément à la loi l’utilisateur disposera d’un droit d’accès, de
        rectification, de suppression, de portabilité et d’opposition au
        traitement des informations personnelles le concernant. Il aura
        également la possibilité de s’opposer au traitement à des fins de
        prospection, notamment commerciale.
        <br /> Pour exercer ce droit plusieurs méthodes sont possible :
        <br />
        <List styleType="disc">
          <ListItem>
            En s'adressant au service de contact via le formulaire concerné sur
            le site ou à l’email suivant contact@gusto-coffee.com
          </ListItem>
          <ListItem>
            Par voie postal en écrivant un courrier à l’adresse suivante :
            Gusto-Coffee, 6 Rue pierre brossolette 75005 Paris
          </ListItem>
          <ListItem>Par la rubrique “Mon compte” sur le site internet</ListItem>
        </List>
        Lors de la navigation sur le site et afin de pouvoir offrir les services
        proposés sur celui-ci, le site de Gusto-coffee peut être amenés à
        collecter des données personnelles vous concernant. Avant chaque
        collecte de données à caractère personnel, le consentement de
        l’utilisateur sera demandé par le biais d’un formulaire à compléter et
        d’une case à cocher après avoir pris connaissance de notre politique de
        protection des données. <br /> Le site collecte des données à caractères
        personnelles dans l’objectif de répondre aux finalités suivantes :
        <List styleType="disc">
          <ListItem>
            Créer des comptes clients afin de permettre la prise en compte des
            commandes et réservations (en ligne), gérer et traiter les commandes
            et réservations, communiquer avec l’utilisateur sur cette gestion
          </ListItem>
          <ListItem>
            Mettre en contact les différents utilisateurs de la plateforme à
            leur demande
          </ListItem>
          <ListItem>
            Assurer le suivi de la relation client notamment dans la gestion des
            réclamations et des demandes ;
          </ListItem>
          <ListItem>
            Gérer les demandes de droit d’accès, de rectification, d’opposition
            des données
          </ListItem>
        </List>
        Gusto-coffee s’engage à collecter des données en se limitant aux données
        strictement nécessaires à la finalité du traitement envisagé, afin de
        pouvoir traiter au mieux les demandes et offrir les services proposés
        sur notre site Internet.
        <br /> Les données à caractère personnel collectées sont conservées
        pendant une durée qui n’excède pas la durée nécessaire aux finalités
        pour lesquelles elles ont été collectées et au maximum pour une durée de
        3 ans à compter de la collecte de données.
        <br /> A l’issue, dès lors que les données collectées ne sont plus
        nécessaires à la finalité pour laquelle elles avaient été collectées
        Gusto-coffee s’engage à les supprimer.
        <br /> Les données à caractère personnelle collectées, concernant
        l’utilisateur, sont destinées à la société Gusto-coffee.
        <br /> Afin de traiter convenablement les données collectées, celles-ci
        seront partagé en autorisant leur accès aux personnels habilités. Les
        personnels concernés sont :
        <List styleType="disc">
          <ListItem>Les prestataires informatiques</ListItem>
          <ListItem>
            Les services internes à savoir le service juridique, financier,
            développement, achats et marketing.
          </ListItem>
        </List>
        La sécurité et la confidentialité des données personnelles une priorité
        pour la société Gusto-coffee.
        <br /> Gusto-coffee se réserve le droit de modifier ladite “politique de
        protection des données” partiellement ou totalement, à tout moment tout
        en conservant sa conformité avec les éventuelles modifications des
        dispositions législatives et réglementaires françaises et européennes.
      </Text>
    </Flex>
  );
};

export default Legals;
