import React from "react";
import { FormControl, Input, Button } from "@chakra-ui/core";
import { useAuth } from "../../services/AuthProvider";
import { Field, Formik, Form } from "formik";
import FormLabel from "@chakra-ui/core/dist/FormLabel";
import FormErrorMessage from "@chakra-ui/core/dist/FormErrorMessage";
import { useHistory } from "react-router-dom";

export function Login({ fromCart }) {
  let history = useHistory();

  const { signin } = useAuth();

  return (
    <Formik
      initialValues={{
        email: "",
        password: "",
      }}
      onSubmit={(values, actions) => {
        const successLogin = signin(JSON.stringify(values));
        console.log("successLogin", successLogin);
        actions.setSubmitting(false);
        if (fromCart) {
          history.push("/cart");
        } else history.push("/user");
      }}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit} display="flex">
          <Field name="email">
            {({ field, form }) => (
              <FormControl isInvalid={form.errors.email && form.touched.email}>
                <FormLabel isRequired htmlFor="email">
                  Email :
                </FormLabel>
                <Input {...field} isRequired id="email" margin="5%" />
                <FormErrorMessage>{form.errors.email}</FormErrorMessage>
              </FormControl>
            )}
          </Field>
          <Field name="password">
            {({ field, form }) => (
              <FormControl
                isInvalid={form.errors.password && form.touched.password}
              >
                <FormLabel isRequired htmlFor="plainPassword">
                  Mot de passe :
                </FormLabel>
                <Input
                  {...field}
                  isRequired
                  type="password"
                  id="password"
                  margin="5%"
                />
                <FormErrorMessage>{form.errors.password}</FormErrorMessage>
              </FormControl>
            )}
          </Field>

          <Button
            mt={4}
            height="35px"
            width="100px"
            border="3px solid"
            borderColor="primary"
            color="primary"
            backgroundColor="main"
            marginTop="15px"
            isLoading={props.isSubmitting}
            className="button"
            type="submit"
          >
            Valider
          </Button>
        </Form>
      )}
    </Formik>
  );
}
