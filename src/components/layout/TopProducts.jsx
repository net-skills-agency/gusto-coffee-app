import React, { useEffect, useState } from "react";
import { Box, Flex } from "@chakra-ui/core";
import Axios from "axios";
import ProductMiniature from "./ProductMiniature";
import {AuthStr} from "../constant/HomeConst";

const TopProducts = ({ save }) => {
  const [topProducts, setTopProducts] = useState([]);
  const FetchProducts = () => {
    Axios.get("https://gustocoffee-staging.herokuapp.com/api/categories").then(
      ({ data }) => {
        const products = [];
        const newProduct = [];
        data.forEach((element) => {
          element.articles.forEach((article) => {
            Object.defineProperty(article, "quantity", {
              value: 1,
              writable: true,
              enumerable: true,
            });
            products.push(article);
          });
        });
        for (let i = 0; i < 4; i++) {
          const index = getRandomInt(products.length);
          newProduct.push(products[index]);
        }

        setTopProducts(newProduct);
      }
    );
  };
  const getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  };
  useEffect(() => {
    FetchProducts();
  }, []);

  return (
    <Flex
      direction="row"
      flexWrap="wrap"
      className="row space-around"
      width="80%"
      justifyContent="space-around"
    >
      {topProducts.map((product) => (
        <Box key={product.id}>
          <ProductMiniature product={product} save={save} />
        </Box>
      ))}
    </Flex>
  );
};

export default TopProducts;
