import React, { useEffect } from "react";
import {
  Flex,
  Icon,
  Heading,
  Button,
  Box,
  FormControl,
  Input,
  Select,
} from "@chakra-ui/core";
import { useState } from "react";
import Plan from "./booking/plan";
import { NavLink, useHistory } from "react-router-dom";
import Axios from "axios";
import { AuthStr } from "../constant/HomeConst";

const Booking = ({ save }) => {
  const history = useHistory();
  const [seats, setSeats] = useState([]);
  const FetchSeat = () => {
    Axios.get(
      "https://gustocoffee-staging.herokuapp.com/api/public-room/1"
    ).then(({ data }) => {
      let seats = data.seats;
      seats.forEach((seat) => {
        seat.status.replace(" ", "_");
      });
      setSeats(seats);
    });
  };
  useEffect(() => {
    FetchSeat();
  }, []);
  const [booking, setBooking] = useState({
    seats: 0,
    dateStart: "",
    dateEnd: "",
  });
  const [step, setStep] = useState(false);
  const [date, setDate] = useState();
  const onDateChange = (value) => {
    setDate(value);
  };
  const hours = [
    { value: "08:00:00", name: "8h00" },
    { value: "09:00:00", name: "9h00" },
    { value: "10:00:00", name: "10h00" },
    { value: "11:00:00", name: "11h00" },
    { value: "12:00:00", name: "12h00" },
    { value: "13:00:00", name: "13h00" },
    { value: "14:00:00", name: "14h00" },
    { value: "15:00:00", name: "15h00" },
    { value: "16:00:00", name: "16h00" },
    { value: "17:00:00", name: "17h00" },
    { value: "18:00:00", name: "18h00" },
    { value: "19:00:00", name: "19h00" },
  ];

  const [hourStart, setHourStart] = useState();
  const onHourStartChange = (value) => {
    setHourStart(value);
    const dateStart = date + " " + value;
    setBooking({ ...booking, dateStart });
  };

  const onHourEndChange = (value) => {
    const dateEnd = date + " " + value;
    setBooking({ ...booking, dateEnd });
  };

  let startHours = hours.slice(0, 11);
  let endHours = hours.slice(
    hours.findIndex((hour) => hour.value === hourStart) + 1,
    12
  );
  const handleSubmit = () => {
    save(booking);
    history.push("/Cart");
  };

  /*
    const [checkedAfternoon, setCheckedAfternoon] = React.useState([false, false, false, false, false, false]);
    const morningHours = ["7h00 - 8h00", "8h00 - 9h00", "9h00 - 10h00", "10h00 - 11h00", "11h00 - 12h00", "12h00 - 13h00"]
    const afternoonHours = ["13h00 - 14h00", "14h00 - 15h00", "15h00 - 16h00", "16h00 - 17h00", "17h00 - 18h00", "18h00 - 19h00"]
    const [checkedMorning, setCheckedMorning] = React.useState([false, false, false, false, false, false]);

    const allMorningChecked = checkedMorning.every(Boolean);
    const allAfternoonChecked = checkedAfternoon.every(Boolean);

    const isIndeterminateMorning = checkedMorning.some(Boolean) && !allMorningChecked;
    const isIndeterminateAfternoon = checkedAfternoon.some(Boolean) && !allAfternoonChecked;
    const handleMorningChange = (e, i) => {
        let newMorning = [...checkedMorning]
        newMorning.splice(i, 1, e.target.checked)
        setCheckedMorning(newMorning)
    }
    const handleAfternoonChange = (e, i) => {
        let newAfternoon = [...checkedAfternoon]
        newAfternoon.splice(i, 1, e.target.checked)
        setCheckedAfternoon(newAfternoon)
    } */

  return (
    <Flex direction="column" className="mt-130 bg-reverse" alignItems="center">
      <Flex
        justifyContent="space-around"
        alignItems="center"
        width="100%"
        marginBottom="30px"
      >
        <NavLink to="/Coworking">
          <Icon
            name="arrow-back"
            size="46px"
            color="#707070"
            stroke="#707070"
          />
          Retour
        </NavLink>
        <Flex direction="row">
          <Heading as="h4">Date&nbsp;:</Heading>
          <Input
            type="date"
            value={date}
            onChange={({ currentTarget }) => onDateChange(currentTarget.value)}
          />
        </Flex>
        <Box></Box>
      </Flex>
      <Flex width="80%">
        <Plan seats={seats} setBooking={setBooking} booking={booking} />
        {step && (
          <Flex
            direction="column"
            zIndex="50"
            position="absolute"
            top="15vh"
            left="10%"
            shadow="md"
            backgroundColor="beige"
            width="80%"
            padding="5%"
            alignItems="center"
            className="lg-60 top-40vh left-20"
          >
            {/* <Heading as="h5" marginBottom="45px">Veuillez selectionner votre/vos créneaux désirés</Heading>
                    <Flex direction="column" className="row space-around" width="100%">
                        <Flex direction="column" >
                            <Checkbox
                                isChecked={allMorningChecked}
                                isIndeterminate={isIndeterminateMorning}
                                onChange={e => setCheckedMorning([e.target.checked, e.target.checked, e.target.checked, e.target.checked, e.target.checked, e.target.checked])}
                                children="Toute la matiné"
                            /> <CheckboxGroup
                                spacing={8}

                            >
                                <Stack pl={6} mt={1} spacing={1}>
                                    {morningHours.map((hour, i) => <Checkbox key={hour}
                                        isChecked={checkedMorning[i]}
                                        onChange={(e) => handleMorningChange(e, i)}
                                        children={hour}
                                    />)}

                                </Stack>
                            </CheckboxGroup>
                        </Flex>
                        <Flex direction="column">
                            <Checkbox
                                isChecked={allAfternoonChecked}
                                isIndeterminate={isIndeterminateAfternoon}
                                onChange={e => setCheckedAfternoon([e.target.checked, e.target.checked, e.target.checked, e.target.checked, e.target.checked, e.target.checked])}
                                children="Toute l'après-midi"
                            /> <CheckboxGroup
                                spacing={8}
                                defaultValue={afternoonHours}
                            >
                                <Stack pl={6} mt={1} spacing={1}>
                                    {afternoonHours.map((hour, i) => <Checkbox key={hour}
                                        isChecked={checkedAfternoon[i]}
                                        onChange={e => handleAfternoonChange(e, i)}
                                        children={hour}
                                    />)}

                                </Stack>
                            </CheckboxGroup>
                        </Flex>

                    </Flex> */}
            <Flex direction="column">
              <Heading as="h5" marginBottom="45px">
                Veuillez selectionner votre créneau
              </Heading>
              <FormControl>
                <label>Heure de début :</label>
                <Select
                  isRequired
                  name="hourStart"
                  placeholder="Choisir une heure de début"
                  onChange={({ currentTarget }) =>
                    onHourStartChange(currentTarget.value)
                  }
                >
                  {startHours.map((hour) => (
                    <option value={hour.value} key={"s" + hour.value}>
                      {hour.name}
                    </option>
                  ))}
                </Select>
                <label>Heure de fin :</label>
                <Select
                  isRequired
                  name="hourEnd"
                  placeholder="Choisir une heure de fin"
                  onChange={({ currentTarget }) =>
                    onHourEndChange(currentTarget.value)
                  }
                >
                  {endHours.map((hour) => (
                    <option value={hour.value} key={"e" + hour.value}>
                      {hour.name}
                    </option>
                  ))}
                </Select>
              </FormControl>
            </Flex>
            <Flex direction="row" justifyContent="space-around" width="100%">
              <Button
                leftIcon="arrow-back"
                marginTop="30px"
                variant="solid"
                onClick={() => setStep(!step)}
              >
                Retour{" "}
              </Button>
              <Button
                height="48px"
                width="200px"
                border="3px solid"
                borderRadius="5px"
                borderColor="primary"
                color="primary"
                backgroundColor="main"
                marginTop="30px"
                className="button"
                onClick={() => handleSubmit()}
              >
                Valider
              </Button>
            </Flex>
          </Flex>
        )}
      </Flex>
      {step ? (
        <Button
          leftIcon="arrow-back"
          marginTop="30px"
          variant="solid"
          onClick={() => setStep(!step)}
        >
          Retour{" "}
        </Button>
      ) : (
        <Button
          height="48px"
          width="200px"
          border="3px solid"
          borderRadius="5px"
          borderColor="primary"
          color="primary"
          backgroundColor="main"
          marginTop="30px"
          className="button"
          onClick={() => setStep(!step)}
          isDisabled={date ? false : true}
        >
          Valider{" "}
        </Button>
      )}
    </Flex>
  );
};

export default Booking;
