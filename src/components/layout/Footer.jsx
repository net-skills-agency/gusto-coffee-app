import React from "react";
import { Flex, Heading, List, ListItem, Text } from "@chakra-ui/core";
import { NavLink } from "react-router-dom";
import { FaTwitter, FaFacebook, FaLinkedin, FaInstagram } from "react-icons/fa";

const Footer = (props) => {
  return (
    <Flex
      direction="column"
      justifyContent="space-between"
      alignItems="center"
      padding="60px 5% 60px"
      marginBottom="60px"
      className=" mb-0"
      backgroundColor="primary"
      color="main"
      fontSize="24px"
      marginTop="60px"
    >
      <Flex
        direction="column"
        justifyContent="space-around"
        alignItems="center"
        className="row mb-0"
        width="100%"
      >
        <List height="250px" marginTop="20px" lineHeight="60px">
          <ListItem>
            <Heading as="h4">Le Coworking</Heading>
          </ListItem>
          <ListItem>
            <NavLink to="/booking">Réserver une place</NavLink>
          </ListItem>
          <ListItem>
            <NavLink to="/booking">Réserver un salon</NavLink>
          </ListItem>
        </List>
        <List height="250px" marginTop="20px" lineHeight="60px">
          <ListItem>
            <Heading as="h4">Le Coffee shop</Heading>
          </ListItem>
          <ListItem>
            <NavLink to="/menu">Boissons chaudes</NavLink>
          </ListItem>
          <ListItem>
            <NavLink to="/menu">Boissons fraiches</NavLink>
          </ListItem>
          <ListItem>
            <NavLink to="/menu">Viennoiseries</NavLink>
          </ListItem>
          <ListItem>
            <NavLink to="/menu">Snacks</NavLink>
          </ListItem>
        </List>
        <List
          height="250px"
          display="flex"
          flexDirection="column"
          justifyContent="space-between"
          marginTop="20px"
        >
          <ListItem>
            <NavLink to="/cgu">
              <Heading as="h4">Conditions Générales de Ventes</Heading>
            </NavLink>
          </ListItem>
          <ListItem>
            <NavLink to="/mentions-legales">
              <Heading as="h4">Mentions Légales</Heading>
            </NavLink>
          </ListItem>
          <ListItem>
            <List display="flex" justifyContent="space-around">
              <ListItem>
                <NavLink to="">
                  <FaTwitter />
                </NavLink>
              </ListItem>
              <ListItem>
                <NavLink to="">
                  <FaFacebook />
                </NavLink>
              </ListItem>
              <ListItem>
                <NavLink to="">
                  <FaLinkedin />
                </NavLink>
              </ListItem>
              <ListItem>
                <NavLink to="">
                  <FaInstagram />
                </NavLink>
              </ListItem>
            </List>
          </ListItem>
        </List>
      </Flex>

      <Text fontSize="12px" marginTop="45px">
        Site réalisé par <NavLink to="">Net Skills Agency</NavLink>
      </Text>
    </Flex>
  );
};

export default Footer;
