import React, { useEffect, useState } from "react";
import "./Header.css";
import ProductMiniature from "./ProductMiniature";
import { Flex, Link, Heading, Image, Text, Spinner } from "@chakra-ui/core";
import Axios from "axios";

export const Menu = ({ save }) => {
  const property = {
    banner: require("../../assets/images/menu.jpg"),
    text:
      "Sit commodo tempor ex consequat incididunt proident quis. Amet minim in nulla ut. Commodo esse ex exercitation anim qui reprehenderit ea.",
  };
  const [loading, setLoading] = useState(true);
  const [categories, setCategories] = useState([
    {
      id: 1,
      designation: "Boissons Chaudes",
      description:
        "Fugiat aute et amet pariatur aute qui officia culpa nulla eiusmod magna tempor laboris. Consequat nulla labore et aliquip ut sit nulla ea. Veniam cillum anim et amet quis nostrud non irure laboris pariatur. Ipsum sit cupidatat dolor incididunt aliquip nostrud incididunt commodo amet sit irure ipsum cillum quis. Velit adipisicing commodo elit cupidatat laboris sit do adipisicing quis. Sit laborum mollit irure laborum.",
      articles: [
        {
          id: 1,
          designation: "Café",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 2,
          designation: "Café",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 3,
          designation: "Café",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 4,
          designation: "Café",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 5,
          designation: "Café",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
      ],
    },
    {
      id: 2,
      designation: "Boissons Froides",
      description:
        "Fugiat aute et amet pariatur aute qui officia culpa nulla eiusmod magna tempor laboris. Consequat nulla labore et aliquip ut sit nulla ea. Veniam cillum anim et amet quis nostrud non irure laboris pariatur. Ipsum sit cupidatat dolor incididunt aliquip nostrud incididunt commodo amet sit irure ipsum cillum quis. Velit adipisicing commodo elit cupidatat laboris sit do adipisicing quis. Sit laborum mollit irure laborum.",
      articles: [
        {
          id: 1,
          designation: "Smoothie",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 2,
          designation: "Smoothie",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 3,
          designation: "Smoothie",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 4,
          designation: "Smoothie",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 5,
          designation: "Smoothie",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
      ],
    },
    {
      id: 3,
      designation: "Pâtisseries",
      description:
        "Fugiat aute et amet pariatur aute qui officia culpa nulla eiusmod magna tempor laboris. Consequat nulla labore et aliquip ut sit nulla ea. Veniam cillum anim et amet quis nostrud non irure laboris pariatur. Ipsum sit cupidatat dolor incididunt aliquip nostrud incididunt commodo amet sit irure ipsum cillum quis. Velit adipisicing commodo elit cupidatat laboris sit do adipisicing quis. Sit laborum mollit irure laborum.",
      articles: [
        {
          id: 1,
          designation: "Croissant",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 2,
          designation: "Croissant",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 3,
          designation: "Croissant",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 4,
          designation: "Croissant",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 5,
          designation: "Croissant",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
      ],
    },
    {
      id: 4,
      designation: "Snacks",
      description:
        "Fugiat aute et amet pariatur aute qui officia culpa nulla eiusmod magna tempor laboris. Consequat nulla labore et aliquip ut sit nulla ea. Veniam cillum anim et amet quis nostrud non irure laboris pariatur. Ipsum sit cupidatat dolor incididunt aliquip nostrud incididunt commodo amet sit irure ipsum cillum quis. Velit adipisicing commodo elit cupidatat laboris sit do adipisicing quis. Sit laborum mollit irure laborum.",
      articles: [
        {
          id: 1,
          designation: "Sandwich",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 2,
          designation: "Sandwich",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 3,
          designation: "Sandwich",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 4,
          designation: "Sandwich",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
        {
          id: 5,
          designation: "Sandwich",
          price: "5€",
          img: "../../assets/images/asset1.png",
        },
      ],
    },
  ]);
  const FecthCategories = () => {
    Axios.get("https://gustocoffee-staging.herokuapp.com/api/categories").then(
      ({ data }) => {
        data.forEach((element) => {
          element.articles.forEach((article) => {
            Object.defineProperty(article, "quantity", {
              value: 1,
              writable: true,
              enumerable: true,
            });
          });
        });
        setCategories(data);
        setLoading(false);
      }
    );
  };
  useEffect(() => {
    FecthCategories();
  }, []);

  if (!loading) {
    return (
      <Flex direction="column">
        <Image src={property.banner} />
        <Flex
          direction="column"
          align="center"
          padding="15% 5%"
          bgSize="contain"
          bgPos="left bottom"
          className="intro bg"
        >
          <Text className="block-text lg-60" marginBottom="45px" shadow="md">
            {property.text}
          </Text>
          <Flex
            justify="space-around"
            width="100%"
            marginTop="10px"
            flexWrap="wrap"
          >
            {categories.map((category) => (
              <Link
                key={category.id}
                href={"#" + category.id}
                _hover={{ textDecoration: "none", fontWeight: "Bold" }}
                margin="0 10px"
              >
                <Heading as="h3" size="lg" fontWeight="inherit">
                  {category.designation}
                </Heading>
              </Link>
            ))}
          </Flex>
        </Flex>
        {categories.map((category) => (
          <Flex direction="column" alignItems="center" key={category.id}>
            <Heading as="h2" id={category.id}>
              {category.designation}
            </Heading>
            <Flex flexWrap="wrap" justifyContent="center">
              {category.articles.map((article) =>
                article.visibility.label === "visible" ? (
                  <ProductMiniature
                    key={article.id}
                    product={article}
                    save={save}
                  />
                ) : null
              )}
            </Flex>
          </Flex>
        ))}
      </Flex>
    );
  } else return <Spinner />;
};
