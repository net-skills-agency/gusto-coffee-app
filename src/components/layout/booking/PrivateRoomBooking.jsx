import { Flex, Image, Box, Heading, Input, FormControl, Select, Button } from '@chakra-ui/core';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

const PrivateRoomBooking = ({ save }) => {
    const history = useHistory();
    const property = {
        banner: require("../../../assets/images/coworking-banner.jpg"),
        salleImg: require("../../../assets/images/salle.jpg"),
        background: "url('../../../assets/images/background_accueil.png')",
        backgroundReverse:
            "url('../../../assets/images/background_accueil-reverse.png')",
    };
    const hours = [
        { value: "08:00:00", name: "8h00" },
        { value: "09:00:00", name: "9h00" },
        { value: "10:00:00", name: "10h00" },
        { value: "11:00:00", name: "11h00" },
        { value: "12:00:00", name: "12h00" },
        { value: "13:00:00", name: "13h00" },
        { value: "14:00:00", name: "14h00" },
        { value: "15:00:00", name: "15h00" },
        { value: "16:00:00", name: "16h00" },
        { value: "17:00:00", name: "17h00" },
        { value: "18:00:00", name: "18h00" },
        { value: "19:00:00", name: "19h00" },
    ]
    const [salon, setSalon] = useState([])
    const FetchSalon = () => {
        Axios.get("https://gustocoffee-staging.herokuapp.com/api/private-rooms").then(({ data }) => setSalon(data))
    }

    useEffect(() => { FetchSalon() }, [])

    const handleSalonChoose = (id) => {
        SetBooking({ ...booking, privateRoom: id });
    }

    const [salonCapacity, setSalonCapacity] = useState();
    const [step, setStep] = useState(1);
    const [booking, SetBooking] = useState({
        privateRoom: 1,
        dateStart: "",
        dateEnd: ""
    })

    const [hourStart, setHourStart] = useState();
    const onHourStartChange = (value) => {
        setHourStart(value);
    }
    const [hourEnd, setHourEnd] = useState();
    const onHourEndChange = (value) => {
        setHourEnd(value);

    }
    let startHours = hours.slice(0, 11)
    let endHours = hours.slice(hours.findIndex((hour) => hour.value === hourStart) + 1, 12)
    const [date, setDate] = useState();
    const onDateChange = (value) => {
        setDate(value);
    }
    const onSalonCapacityChange = (currentTarget) => {
        setSalonCapacity(currentTarget.value);
    }
    const handleSubmit = () => {
        save(booking)
        history.push("/Cart")

    }
    return (<Flex direction="column" alignItems="center">
        <Box width="100vw" >
            <Image src={property.banner} alt="Bannière Coworking" />
        </Box>
        <Flex width="80%" justifyContent="center">
            {step === 1 && <Flex direction="column">
                <Heading as="h4">Date&nbsp;:</Heading>
                <Input
                    type="date"
                    value={date}
                    onChange={({ currentTarget }) => onDateChange(currentTarget.value)}
                />
                <Button isDisabled={!date} height="48px" width="200px" border="3px solid" borderColor="primary" color="primary" backgroundColor="main" marginTop="15px" className="button" onClick={() => setStep(2)}>Valider</Button>

            </Flex>}
            {step === 2 && <Flex direction="column" alignItems="center">
                <Heading as="h4">Veuillez selectionner votre créneau</Heading>
                <FormControl>
                    <label>Heure de début :</label>
                    <Select isRequired name="hourStart" value={hourStart} placeholder="Choisir une heure de début" onChange={({ currentTarget }) => onHourStartChange(currentTarget.value)}>
                        {startHours.map(hour => <option value={hour.value} key={"s" + hour.value}>{hour.name}</option>)}
                    </Select>
                    <label>Heure de fin :</label>
                    <Select isRequired name="hourEnd" value={hourEnd} placeholder="Choisir une heure de fin" onChange={({ currentTarget }) => onHourEndChange(currentTarget.value)}>
                        {endHours.map(hour => <option value={hour.value} key={"e" + hour.value}>{hour.name}</option>)}
                    </Select>
                </FormControl>
                <Button leftIcon="arrow-back" marginTop="30px" variant="solid" onClick={() => setStep(1)}>Retour </Button>
                <Button
                    isDisabled={!hourStart || !hourEnd}
                    height="48px"
                    width="200px"
                    border="3px solid"
                    borderRadius="5px"
                    borderColor="primary"
                    color="primary"
                    backgroundColor="main"
                    marginTop="30px"
                    className="button"
                    onClick={() => {
                        setStep(3); const dateStart = date + " " + hourStart
                        const dateEnd = date + " " + hourEnd
                        SetBooking({ ...booking, dateStart, dateEnd })
                    }}>Valider</Button>
            </Flex>}

            {step === 3 && <Flex direction="column" alignItems="center">
                <Heading as="h4">Choix du Salon&nbsp;:</Heading>
                <label>nombre de place du salon :</label>
                <Select isRequired name="salon" value={salonCapacity} placeholder="Choisir un nombre de place" onChange={({ currentTarget }) => onSalonCapacityChange(currentTarget)}>
                    <option value="4">Quatre Places</option>
                    <option value="6">Six Places</option>
                </Select>
                {salonCapacity === "4" && <Select placeholder="Choisir votre salon" onChange={({ currentTarget }) => handleSalonChoose(currentTarget.value)}>
                    {salon.map((s) => s.capacity === 4 && <option value={s.id} key={s.id}>{s.designation}</option>)}
                </Select>}
                {salonCapacity === "6" && <Select placeholder="Choisir votre salon" onChange={({ currentTarget }) => handleSalonChoose(currentTarget.value)}>
                    {salon.map((s) => s.capacity === 6 && <option value={s.id} key={s.id}>{s.designation}</option>)}
                </Select>}
                <Button leftIcon="arrow-back" marginTop="30px" variant="solid" onClick={() => setStep(2)}>Retour </Button>
                <Button
                    height="48px"
                    width="200px"
                    border="3px solid"
                    borderRadius="5px"
                    borderColor="primary"
                    color="primary"
                    backgroundColor="main"
                    marginTop="30px"
                    className="button"
                    onClick={() => handleSubmit()}>Valider </Button>
            </Flex>}
        </Flex>

    </Flex>);
}

export default PrivateRoomBooking;