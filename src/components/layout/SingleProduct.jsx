import React, { useEffect, useState } from 'react';
import {
    Button, Flex, Heading, Image, Text, NumberInput,
    NumberInputField,
    NumberInputStepper,
    NumberIncrementStepper,
    NumberDecrementStepper,
} from '@chakra-ui/core';
import { useParams } from 'react-router-dom';
import Axios from 'axios';
import TopProducts from "./TopProducts";
import {AuthStr} from "../constant/HomeConst";

const SingleProduct = ({ save }) => {
    const banner = require('../../assets/images/menu.jpg');
    const id = useParams().id;
    const [product, setProduct] = useState({ id, designation: "", description: "", price: "", img: "", quantity: 1 });

    const handleQuantityChange = (value) => {
        let newProduct = { ...product, quantity: value }
        setProduct(newProduct)
    }

    const handleAddToCart = (product) => {
        save(product)
    }
    const FetchProduct = async (id) => {
        await Axios.get("https://gustocoffee-staging.herokuapp.com/api/article/" + id).then(data => {
            let product = data.data;
            Object.defineProperty(product, 'quantity', { value: 1, writable: true });
            setProduct(product)
        })

    }
    useEffect(() => { FetchProduct(id) }, [id])
    return (<Flex direction="column" alignItems="center">
        <Image src={banner} />
        <Heading as="h2">Autres produits liés :</Heading>
        <TopProducts />
        <Flex direction="column" className="row intro bg" justifyContent="space-around" alignItems="center" width="100%">
            <Image src={`https://gustocoffee-staging.herokuapp.com/images/article/${product.lien_image}`} alt={product.name} display="block" />
            <Flex direction="column" padding="5%" shadow="md" backgroundColor="main" justifyContent="space-around"   >
                <Heading as="h2">{product.designation}</Heading>
                <Text >{product.description}</Text>
                <Heading as="h3" textAlign="center">{product.price} €</Heading>
                <NumberInput min={0} max={30} value={product.quantity} onChange={(value) => handleQuantityChange(value)}>
                    <NumberInputField type="number" />
                    <NumberInputStepper>
                        <NumberIncrementStepper />
                        <NumberDecrementStepper />
                    </NumberInputStepper>
                </NumberInput>
                <Button height="48px"
                    width="200px"
                    border="3px solid"
                    borderRadius="5px"
                    borderColor="primary"
                    color="primary"
                    backgroundColor="main"
                    marginTop="30px"
                    className="button"
                    onClick={() => handleAddToCart(product)}> Ajouter au panier</Button>
            </Flex>
        </Flex>
    </Flex >);
}

export default SingleProduct;
