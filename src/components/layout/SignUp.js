import React from "react";
import { Button, FormControl, Input, useToast } from "@chakra-ui/core";
import { Field, Formik, Form } from "formik";
import FormErrorMessage from "@chakra-ui/core/dist/FormErrorMessage";
import FormLabel from "@chakra-ui/core/dist/FormLabel";
import { useAuth } from "../../services/AuthProvider";
import { useHistory } from "react-router-dom";

export function SignUp({ fromCart }) {
  const { signup, signin } = useAuth();
  const history = useHistory();
  const succesToast = useToast();

  return (
    <Formik
      initialValues={{
        firstname: "",
        lastname: "",
        email: "",
        plainPassword: "",
        passwordCheck: "",
      }}
      onSubmit={(values, actions) => {
        const successSignUp = signup(JSON.stringify(values));
        console.log("successLogin", successSignUp);
        actions.setSubmitting(false);
        const successLogin = signin(JSON.stringify(values));
        console.log("successLogin", successLogin);
        if (fromCart) {
          history.push("/cart");
        } else history.push("/user");
      }}
    >
      {(props) => (
        <Form onSubmit={props.handleSubmit}>
          <Field name="firstname">
            {({ field, form }) => (
              <FormControl
                isInvalid={form.errors.firstname && form.touched.firstname}
              >
                <FormLabel isRequired htmlFor="firstName">
                  Prénom :
                </FormLabel>
                <Input {...field} isRequired id="firstName" margin="5%" />
                <FormErrorMessage>{form.errors.firstname}</FormErrorMessage>
              </FormControl>
            )}
          </Field>
          <Field name="lastname">
            {({ field, form }) => (
              <FormControl
                isInvalid={form.errors.lastname && form.touched.lastname}
              >
                <FormLabel isRequired htmlFor="name">
                  Nom :
                </FormLabel>
                <Input {...field} isRequired id="name" margin="5%" />
                <FormErrorMessage>{form.errors.lastname}</FormErrorMessage>
              </FormControl>
            )}
          </Field>

          <Field name="email">
            {({ field, form }) => (
              <FormControl isInvalid={form.errors.email && form.touched.email}>
                <FormLabel isRequired htmlFor="email">
                  Email :
                </FormLabel>
                <Input {...field} isRequired margin="5%" />
                <FormErrorMessage>{form.errors.email}</FormErrorMessage>
              </FormControl>
            )}
          </Field>
          <Field name="plainPassword">
            {({ field, form }) => (
              <FormControl
                isInvalid={
                  form.errors.plainPassword && form.touched.plainPassword
                }
              >
                <FormLabel isRequired htmlFor="plainPassword">
                  Mot de passe :
                </FormLabel>
                <Input {...field} isRequired type="password" margin="5%" />
                <FormErrorMessage>{form.errors.plainPassword}</FormErrorMessage>
              </FormControl>
            )}
          </Field>
          <Button
            mt={4}
            height="35px"
            width="100px"
            border="3px solid"
            borderColor="primary"
            color="primary"
            backgroundColor="main"
            marginTop="15px"
            className="button"
            isLoading={props.isSubmitting}
            type="submit"
          >
            Valider
          </Button>
        </Form>
      )}
    </Formik>
  );
}
