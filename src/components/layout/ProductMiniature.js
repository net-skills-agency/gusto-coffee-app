import React from "react";
import { Flex, Image, Text, Heading, Button } from "@chakra-ui/core";
import "./ProductMiniature.css";
import { NavLink } from "react-router-dom";

const ProductMiniature = ({ product, save }) => {
  return (
    <Flex
      position="relative"
      direction="column"
      justifyContent="flex-end"
      alignItems="center"
      margin="10px"
      className="productMiniature"
    >
      <NavLink to={"/produit/" + product.id}>
        <Image
          size="150px"
          src={`https://gustocoffee-staging.herokuapp.com/images/article/${product.lien_image}`}
          fallbackSrc="https://via.placeholder.com/150"
          alt={product.name}

        />
      </NavLink>
      <Flex
        display="flex"
        direction="column"
        position="absolute"
        alignItems="center"
        className="productMiniatureInfos"
      >
        <NavLink to={"/produit/" + product.id}>
          <Heading as="h3" size="md">
            {product.designation}
          </Heading>
          <Text textAlign="center">{product.price} €</Text>
        </NavLink>
        <Button onClick={() => save(product)}>Ajouter au panier</Button>
      </Flex>
    </Flex>
  );
};
export default ProductMiniature;
