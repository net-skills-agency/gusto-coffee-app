import React from "react";
import {
  Flex,
  Heading,
  Image,
  Tab,
  TabList,
  TabPanel,
  Tabs,
  TabPanels,
} from "@chakra-ui/core";
import { Login } from "./Login";
import { SignUp } from "./SignUp";
import "./Connexion.css";

export function Connexion({ fromCart }) {
  const property = {
    logo: require("../../assets/images/logo_gusto-coffe-dark.png"),
  };

  return (
    <Flex
      height="100%"
      direction="column"
      align="center"
      className="bg-salle mt-100"
      justify="space-around"
      padding="10% 5%"
    >
      <Image src={property.logo} size="250px" marginBottom="30px" />

      <Tabs variant="unstyled" backgroundColor="main" isFitted>
        <TabList justify="center">
          <Tab
            boxShadow="0 0 3px #1e4e59da"
            _selected={{ boxShadow: "none" }}
            _hover={{ boxShadow: "0 0 5px #1e4e59da" }}
            _active={{ boxShadow: "0 0 3px #1e4e59da" }}
          >
            <Heading as="h2" size="lg">
              Connexion
            </Heading>
          </Tab>
          <Tab
            boxShadow="0 0 3px #1e4e59da"
            _selected={{ boxShadow: "none" }}
            _hover={{ boxShadow: "0 0 5px #1e4e59da" }}
            _active={{ boxShadow: "0 0 3px #1e4e59da" }}
          >
            <Heading as="h2" size="lg">
              Inscription
            </Heading>
          </Tab>
        </TabList>
        <TabPanels className="bg-connexion">
          <TabPanel padding="80px 100px">
            <Login fromCart={fromCart} />
          </TabPanel>
          <TabPanel padding="50px 75px">
            <SignUp fromCart={fromCart} />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Flex>
  );
}
