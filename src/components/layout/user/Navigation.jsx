import React from "react";
import { NavLink } from "react-router-dom";
import { Avatar, Button, Flex } from "@chakra-ui/core";
import { useAuth } from "../../../services/AuthProvider";
import { useHistory } from "react-router-dom";

const Navigation = (props) => {
  const { signout } = useAuth();
  const history = useHistory();
  const signOut = () => {
    signout();
    history.push("/");
  };

  return (
    <Flex
      direction="column"
      alignItems="center"
      width="100%"
      justifyContent="space-between"
    >
      <Avatar name="Dan Abrahmov" src="https://bit.ly/dan-abramov" />
      <Flex
        borderTop={"1px"}
        width="100%"
        justifyContent="center"
        padding="20px"
      >
        <NavLink
          to="/user"
          _hover={{
            textDecoration: "none",
            fontWeight: "Bold",
          }}
        >
          {" "}
          Tableau de bord
        </NavLink>
      </Flex>
      <Flex
        borderTop={"1px"}
        width="100%"
        justifyContent="center"
        padding="20px"
      >
        <NavLink
          to="/user/profil"
          _hover={{
            textDecoration: "none",
            fontWeight: "Bold",
          }}
        >
          {" "}
          Profil
        </NavLink>
      </Flex>
      <Flex
        borderTop={"1px"}
        width="100%"
        justifyContent="center"
        padding="20px"
      >
        <NavLink
          to="/user/history"
          _hover={{
            textDecoration: "none",
            fontWeight: "Bold",
          }}
        >
          {" "}
          Historique
        </NavLink>
      </Flex>
      <Flex
        borderTop={"1px"}
        width="100%"
        justifyContent="center"
        padding="20px"
      >
        <NavLink
          to="/user/members"
          _hover={{
            textDecoration: "none",
            fontWeight: "Bold",
          }}
        >
          {" "}
          Membres
        </NavLink>
      </Flex>
      <Flex
        borderTop={"1px"}
        width="100%"
        justifyContent="center"
        padding="20px"
        onClick={signOut}
      >
        <Button>Déconnexion</Button>
      </Flex>
    </Flex>
  );
};

export default Navigation;
