import Axios from "axios";
import React, { useEffect, useState } from "react";
import {AuthStr} from "../../constant/HomeConst";

const OrderHistory = (props) => {
  const [orders, setOrders] = useState();

  const FetchOrders = () => {
    Axios.get(
      "https://gustocoffee-staging.herokuapp.com/api/user/orders",{ headers: { Authorization: AuthStr } }
    ).then(({ data }) => setOrders(data));
  };
  useEffect(() => {
    FetchOrders();
  }, []);
  return (
    <table className="table table-hover">
      <thead>
        <tr>
          <th scope="col">Résumé</th>
        </tr>
      </thead>
      <tbody>
        {orders && orders.map((order) => (
          <tr className="table-active" key={order.id}>
            {order.products.map((product) => (
              <td>
                {"Commande n° " + order.id}
                <br />
                {product.designation + " x" + product.quantity}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default OrderHistory;
