import React, { useState } from 'react';
import { Box, IconButton, Flex, Heading } from '@chakra-ui/core';
import Navigation from './Navigation';

import BookingHistory from './BookingHistory';
import OrderHistory from "./OrderHistory"

const History = (props) => {
    let screen = window.innerWidth;
    const [navigationVisibility, setNavigationVisibility] = useState(false);

    return (
        <Flex className="mt-130">
            {screen >= 1024 ? <Box width="20%" backgroundColor="main" border="2px solid" borderColor="primary" height="100vh" shadow="md">
                <Navigation />
            </Box>
                : !navigationVisibility ? <Box width="90%" position="absolute" zIndex="100" backgroundColor="main" left="-90%" border="2px solid" borderColor="primary" height="100vh">
                    <Navigation />
                    <IconButton className="display-nav" icon="chevron-right" position="absolute" top="50%" right="-40px" variant="outline" variantColor="teal" border={'2px'} onClick={() => setNavigationVisibility(true)} />
                </Box> : <Box width="90%" position="absolute" zIndex="100" backgroundColor="main" left="0" border="2px solid" borderColor="primary" height="100vh">
                        <Navigation />
                        <IconButton className="display-nav" icon="chevron-left" position="absolute" top="50%" right="-40px" variant="outline" variantColor="teal" border={'2px'} onClick={() => setNavigationVisibility(false)} />
                    </Box>}
            <Flex direction="column" alignItems="center" width="100%">
                <Flex direction="column" className="row space-around" width="100%">
                    <Box>
                        <Heading as="h3">Dernière Commande</Heading>
                        <OrderHistory />
                    </Box>
                    <Box>
                        <Heading as="h3">Dernière Réservation</Heading>
                        <BookingHistory />
                    </Box>
                </Flex>
            </Flex>
        </Flex>);
}

export default History;