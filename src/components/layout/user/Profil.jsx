import React, { useState } from "react";
import {
  Flex,
  Box,
  Button,
  IconButton,
  Heading,
  FormControl,
  Textarea,
  Input,
  Icon,
} from "@chakra-ui/core";
import Navigation from "./Navigation";
import useCookies from "react-cookie/cjs/useCookies";

const Profil = (props) => {
  const [cookie] = useCookies(['user']);
  const user = cookie.user.user;
  let screen = window.innerWidth;
  const [navigationVisibility, setNavigationVisibility] = useState(false);

  return (
    <Flex className="mt-130">
      {screen >= 1024 ? (
        <Box
          width="20%"
          backgroundColor="main"
          border="2px solid"
          borderColor="primary"
          height="100vh"
          shadow="md"
        >
          <Navigation />
        </Box>
      ) : !navigationVisibility ? (
        <Box
          zIndex="100"
          width="90%"
          position="fixed"
          backgroundColor="main"
          left="-90%"
          border="2px solid"
          borderColor="primary"
          height="100vh"
        >
          <Navigation />
          <IconButton
            className="display-nav"
            icon="chevron-right"
            position="absolute"
            top="50%"
            right="-40px"
            variant="outline"
            variantColor="teal"
            border={"2px"}
            onClick={() => setNavigationVisibility(true)}
          />
        </Box>
      ) : (
        <Box
          zIndex="100"
          width="90%"
          position="fixed"
          backgroundColor="main"
          left="0"
          border="2px solid"
          borderColor="primary"
          height="100vh"
        >
          <Navigation />
          <IconButton
            className="display-nav"
            icon="chevron-left"
            position="absolute"
            top="50%"
            right="-40px"
            variant="outline"
            variantColor="teal"
            border={"2px"}
            onClick={() => setNavigationVisibility(false)}
          />
        </Box>
      )}

      <Flex
        direction="column"
        width="100%"
        alignItems="center"
        justifyContent="center"
      >
        <Flex
          direction="column"
          className="row space-around"
          alignItems="center"
          width="100%"
        >
          <Flex direction="column">
            <FormControl>
              <Heading as="h3">Informations Personnelles</Heading>
              <Flex direction="column">
                <Input
                  name="firstname"
                  type="text"
                  placeholder={user.firstname}
                />
                <Input
                  name="lastname"
                  type="text"
                  placeholder={user.lastname}
                />
                <Input name="email" type="email" placeholder={user.email} />
                <Textarea placeholder="Description" />
              </Flex>
              <Flex direction="column" alignItems="center">
                {/*   <Input
                  name="twitter"
                  type="text"
                  placeholder="Compte Linkedin"
                />
                <Input
                  name="linkedin"
                  type="text"
                  placeholder="Compte Twitter"
                />*/}
                <Icon name="lock" size="24px" margin="20px 0" />

                <Input
                  name="password"
                  type="password"
                  placeholder="Mot de passe"
                />

                <Input
                  name="password_check"
                  type="password"
                  placeholder="Vérification de votre mot de passe"
                />
              </Flex>
            </FormControl>
          </Flex>
          <Flex direction="column">
            <FormControl>
              <Heading as="h3">Informations Professionnelles</Heading>
              <Flex direction="column">
                <Input name="compagny" type="text" placeholder="Société" />
                <Input name="url" type="text" placeholder="Site Web" />
                <Input
                  name="activity"
                  type="text"
                  placeholder="Domaine d'activité"
                />
                <Input
                  name="expert"
                  type="text"
                  placeholder="Domaine d'expertise"
                />
              </Flex>
            </FormControl>
          </Flex>
        </Flex>

        <Button
          height="48px"
          width="200px"
          border="3px solid"
          borderRadius="5px"
          borderColor="primary"
          color="primary"
          backgroundColor="main"
          marginTop="30px"
          className="button"
        >
          Envoyer
        </Button>
      </Flex>
    </Flex>
  );
};

export default Profil;
