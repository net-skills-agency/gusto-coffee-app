import Axios from "axios";
import React, { useEffect, useState } from "react";
import {AuthStr} from "../../constant/HomeConst";

const BookingHistory = (props) => {
  const [bookings, setBooknigs] = useState();
  const FetchBookings = () => {
    Axios.get(
      "https://gustocoffee-staging.herokuapp.com/api/user/bookings",{ headers: { Authorization: AuthStr } }
    ).then(({ data }) => {
      setBooknigs(data);
    });
  };
  useEffect(() => {
    FetchBookings();
  }, []);
  return (
    <table className="table table-hover">
      <thead>
        <tr>
          <th scope="col">Résumé</th>
          <th scope="col">Dates</th>
        </tr>
      </thead>
      <tbody>
        {bookings && bookings.private.map((salon) => (
          <tr className="table-active" key={salon.id}>
            <td>{salon.private_room.designation}</td>
            <td>{salon.date_start + " au " + salon.date_end}</td>
          </tr>
        ))}
        {bookings && bookings.public.map((salon) => (
          <tr className="table-active" key={salon.id}>
            <td>{salon.seat.designation}</td>
            <td>{salon.date_start + " au " + salon.date_end}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default BookingHistory;
