import React, { useState, useEffect } from "react";
import {
  Flex,
  IconButton,
  Box,
  Avatar,
  Text,
  Link,
  Icon,
  Heading,
} from "@chakra-ui/core";
import Axios from "axios";
import Navigation from "./Navigation";
import {AuthStr} from "../../constant/HomeConst";

const Trombinoscope = (props) => {
  let screen = window.innerWidth;
  const [navigationVisibility, setNavigationVisibility] = useState(false);

  const [members, setMembers] = useState([
    {
      lien_image: "https://bit.ly/code-beast",
      user: {
        firstname: "Christian",
        lastname: "Nwamba",
        job: "Designer",
        email: "",
      },
    },
  ]);
  const FetchMembers = () => {
    Axios.get("https://gustocoffee-staging.herokuapp.com/api/paper-clip",{ headers: { Authorization: AuthStr } })
      .then((data) => {
        console.log(data);
      })
      .catch((error) => console.log('FetchMembers with error :', error));
  };
  useEffect(() => {
    FetchMembers();
  }, []);
  return (
    <Flex className="mt-130" backgroundColor="main">
      {screen >= 1024 ? (
        <Box
          width="20%"
          backgroundColor="main"
          border="2px solid"
          borderColor="primary"
          height="100vh"
          shadow="md"
        >
          <Navigation />
        </Box>
      ) : !navigationVisibility ? (
        <Box
          width="90%"
          position="absolute"
          zIndex="100"
          backgroundColor="main"
          left="-90%"
          border="2px solid"
          borderColor="primary"
          height="100vh"
        >
          <Navigation />
          <IconButton
            className="display-nav"
            icon="chevron-right"
            position="absolute"
            top="50%"
            right="-40px"
            variant="outline"
            variantColor="teal"
            border={"2px"}
            onClick={() => setNavigationVisibility(true)}
          />
        </Box>
      ) : (
        <Box
          width="90%"
          position="absolute"
          zIndex="100"
          backgroundColor="main"
          left="0"
          border="2px solid"
          borderColor="primary"
          height="100vh"
        >
          <Navigation />
          <IconButton
            className="display-nav"
            icon="chevron-left"
            position="absolute"
            top="50%"
            right="-40px"
            variant="outline"
            variantColor="teal"
            border={"2px"}
            onClick={() => setNavigationVisibility(false)}
          />
        </Box>
      )}

      <Flex direction="column" alignItems="center" width="100%">
        <Flex direction="column" width="100%" alignItems="center">
          {/*  <form className="form-inline my-2 my-lg-0">
                        <input className="form-control mr-sm-2" type="text" placeholder="Search" />
                        <IconButton icon="search" className="btn btn-secondary my-2 my-sm-0" type="submit">Search</IconButton>
                    </form> */}
          <Heading as="h2">Les Membres Gusto Coffee</Heading>
          <Flex className="lg-15" width="80%" shadow="lg" flexWrap="wrap">
            {members.map((member, index) => (
              <Flex
                  key={index}
                direction="column"
                rounded="lg"
                backgroundColor="white"
                alignItems="center"
                padding="5%"
              >
                <Avatar
                  name={member.user.firstname + " " + member.user.lastname}
                  src={member.lien_image}
                  size="80 %"
                />
                <Text>
                  {" "}
                  {member.user.firstname + " " + member.user.lastname}
                </Text>
                <Text>{member.user.job}</Text>
                <Link isExternal href={"mailto:" + member.user.email}>
                  <Icon name="email" />
                </Link>
              </Flex>
            ))}
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Trombinoscope;
