import React from "react";
import {
  Flex,
  Textarea,
  Image,
  FormControl,
  Input,
  Button,
  FormLabel,
  Heading,
} from "@chakra-ui/core";

export function Contact() {
  const property = {
    banner: require("../../assets/images/coworking-banner.jpg"),
  };

  return (
    <Flex height="100vh" direction="column">
      <Image src={property.banner} alt="Bannière Coworking" />
      <Flex
        direction="column"
        alignItems="center"
        padding="10% 5%"
        className="bg"
      >
        <FormControl
          isRequired
          display="flex"
          flexDirection="column"
          justifyContent="space-around"
          alignItems="center"
          shadow="md"
          padding="30px 70px"
          backgroundColor="white"
          className="lg-60"
        >
          <Heading as="h2" marginBottom="5%">
            Contact
          </Heading>
          <FormLabel htmlFor="name">Nom</FormLabel>
          <Input type="email" id="email" placeholder="Nom" margin="5%" />
          <FormLabel htmlFor="email">Email</FormLabel>
          <Input type="email" id="email" placeholder="Email" margin="5%" />
          <FormLabel htmlFor="message">Message</FormLabel>
          <Textarea placeholder="Message" />
          <Button
            height="35px"
            width="100px"
            border="3px solid"
            borderColor="primary"
            color="primary"
            backgroundColor="main"
            marginTop="15px"
            className="button"
            type="submit"
          >
            Envoyer
          </Button>
        </FormControl>
      </Flex>
    </Flex>
  );
}
