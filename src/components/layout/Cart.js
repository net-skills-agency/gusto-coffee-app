import React from "react";
import {
  Box,
  Flex,
  Heading,
  Button,
  Image,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from "@chakra-ui/core";
import TopProducts from "./TopProducts";
import { useHistory } from "react-router-dom";
import useAuth from "./../../services/AuthProvider";
import Axios from "axios";
import { AuthStr } from "../constant/HomeConst";
export const Cart = ({
  cart,
  products,
  saveProducts,
  setProducts,
  setFromCart,
}) => {
  const history = useHistory();
  const { user } = useAuth();
  const handleQuantityChange = (id, value) => {
    const index = products.findIndex((product) => product.id === id);
    const newProducts = [...products];
    newProducts[index].quantity = value;
    setProducts(newProducts.filter((element) => element.quantity > 0));
  };
  const handleSubmit = () => {
    if (!user) {
      setFromCart(true);
      history.push("/connexion");
    } else {
      if (cart.rooms.length > 0) {
        cart.rooms.forEach((element) => {
          Axios.post(
            "https://gustocoffee-staging.herokuapp.com/api/private-booking",
            element,
            { headers: { Authorization: AuthStr } }
          ).then((response) => console.log(response));
        });
      } else if (cart.seats.length >= 0) {
        cart.seats.forEach((element) => {
          Axios.post(
            "https://gustocoffee-staging.herokuapp.com/api/public-booking",
            element,
            { headers: { Authorization: AuthStr } }
          ).then((response) => console.log(response));
        });
      } else if (products.length > 0) {
        Axios.post(
          "https://gustocoffee-staging.herokuapp.com/api/orders",
          products,
          { headers: { Authorization: AuthStr } }
        ).then((response) => console.log(response));
      }
    }
  };

  return (
    <Flex direction="column" className="intro bg-reverse" alignItems="center">
      <Heading as="h2">Autres produits liés :</Heading>
      <TopProducts save={saveProducts} />
      <Heading as="h2">Récapitulatif de commande</Heading>
      <Flex
        direction="column"
        alignItems="center"
        backgroundColor="main"
        width="80%"
      >
        {cart.rooms.length > 0 && (
          <Box>
            <Heading as="h3" fontSize="24px">
              Réservation de Salon privé
            </Heading>
            <table>
              <thead>
                <tr>
                  <th>Identifiant de la réservation</th>
                  <th>Date de réservation</th>
                </tr>
              </thead>
              <tbody>
                {cart.rooms.map((room) => (
                  <tr padding="10px 0" key={room.id}>
                    <td>{room.designation}</td>
                    <td>
                      du {room.dateStart} au {room.dateEnd}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Box>
        )}

        {cart.seats.length > 0 && (
          <Box>
            <Heading as="h3">Réservation de place</Heading>
            <table>
              <thead>
                <tr>
                  <th>Identifiant de la réservation</th>
                  <th>Date de réservation</th>
                </tr>
              </thead>
              <tbody>
                {cart.seats.map((seat) => (
                  <tr padding="10px 0" key={seat.id}>
                    <td>{seat.seats[0].id}</td>
                    <td>
                      du {seat.dateStart} au {seat.dateEnd}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Box>
        )}

        {cart.products.length > 0 && (
          <Box>
            <Heading as="h3">Produits</Heading>
            <table>
              <thead>
                <tr>
                  <th></th>
                  <th>Produit</th>
                  <th>Quatité</th>
                  <th>Prix</th>
                </tr>
              </thead>
              <tbody>
                {cart.products.map((product) => (
                  <tr padding="10px 0" key={product.id}>
                    <td>
                      <Image
                        size="200px"
                        src={`https://gustocoffee-staging.herokuapp.com/images/article/${product.lien_image}`}
                        fallbackSrc="https://via.placeholder.com/100"
                      />
                    </td>
                    <td>{product.designation}</td>
                    <td>
                      <NumberInput
                        min={0}
                        value={product.quantity}
                        onChange={(value) =>
                          handleQuantityChange(product.id, value)
                        }
                      >
                        <NumberInputField type="number" />
                        <NumberInputStepper>
                          <NumberIncrementStepper />
                          <NumberDecrementStepper />
                        </NumberInputStepper>
                      </NumberInput>
                    </td>
                    <td>{product.price * product.quantity} €</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Box>
        )}
      </Flex>
      <Flex
        direction="row"
        justifyContent="space-around"
        flexWrap="wrap"
        width="80%"
        alignItems="center"
      >
        <Button
          height="48px"
          width="200px"
          border="3px solid"
          borderRadius="5px"
          borderColor="primary"
          color="primary"
          backgroundColor="main"
          marginTop="30px"
          className="button"
          onClick={() => history.push("/Coworking")}
        >
          Réserver une salle
        </Button>
        <Button
          height="48px"
          width="200px"
          border="3px solid"
          borderRadius="5px"
          borderColor="primary"
          color="primary"
          backgroundColor="main"
          marginTop="30px"
          className="button"
          onClick={() => history.push("/Menu")}
        >
          Continuer mes achats
        </Button>
        <Button
          height="48px"
          width="200px"
          border="3px solid"
          borderRadius="5px"
          borderColor="primary"
          color="primary"
          backgroundColor="main"
          marginTop="30px"
          className="button"
          onClick={() => handleSubmit()}
        >
          Valider ma commande
        </Button>
      </Flex>
    </Flex>
  );
};
