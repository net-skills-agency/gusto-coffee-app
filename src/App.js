import React, { useEffect, useState } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import Cache from "./services/Cache";
import { Header } from "./components/layout/Header";
import Home from "./components/layout/Home";
import { Menu } from "./components/layout/Menu";
import CoWorking from "./components/layout/CoWorking";
import { Contact } from "./components/layout/Contact";
import { Connexion } from "./components/layout/Connexion";
import { Cart } from "./components/layout/Cart";
import { CSSReset, ThemeProvider } from "@chakra-ui/core";
import customTheme from "./theme";
import Booking from "./components/layout/Booking";
import { AuthProvider } from "./services/AuthProvider";
import Footer from "./components/layout/Footer";
import UserBoard from "./components/layout/user/UserBoard";
import Profil from "./components/layout/user/Profil";
import History from "./components/layout/user/History";
import Trombinoscope from "./components/layout/user/Trombiniscope";
import PrivateRoomBooking from "./components/layout/booking/PrivateRoomBooking";
import SingleProduct from "./components/layout/SingleProduct";
import { useCookies } from "react-cookie";
import Legals from "./components/layout/Legals";
import CGU from "./components/layout/cgu";

export default function App({ children }) {
  const [cookie] = useCookies(["user"]);

  const [cart, setCart] = useState({ rooms: [], products: [], seats: [] });
  const [rooms, setRooms] = useState([]);
  const [seats, setSeats] = useState([]);
  const [products, setProducts] = useState([]);
  const [fromCart, setFromCart] = useState(false);

  const SaveSeats = (seat) => {
    let newSeats = [...seats];
    newSeats.push(seat);
    setSeats(newSeats);
  };
  const SaveRooms = (room) => {
    let newRooms = [...rooms];
    newRooms.push(room);
    setRooms(newRooms);
  };
  const SaveProducts = (product) => {
    let newProducts = [...products];
    newProducts.push(product);
    setProducts(newProducts);
  };

  const SaveCart = async () => {
    await setCart({ ...cart, rooms, seats, products });
    Cache.set("cart", { ...cart, cart });
  };

  useEffect(() => {
    SaveCart();
  }, [rooms, seats, products]);

  const FetchCart = () => {
    const localCart = Cache.get("cart");
    if (localCart) {
      setCart(localCart);
    }
  };

  const user = cookie.user;

  return (
    <ThemeProvider theme={customTheme}>
      <AuthProvider>
        <CSSReset />
        {children}
        <Router>
          <Header />
          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/menu">
              <Menu save={SaveProducts} />
            </Route>
            <Route path="/produit/:id">
              <SingleProduct save={SaveProducts} />
            </Route>
            <Route path="/coWorking">
              <CoWorking />
            </Route>
            <Route path="/contact">
              <Contact />
            </Route>
            <Route path="/mentions-legales">
              <Legals />
            </Route>
            <Route path="/cgu">
              <CGU />
            </Route>
            <Route path="/cart">
              <Cart
                cart={cart}
                products={products}
                saveProducts={SaveProducts}
                setProducts={setProducts}
                setFromCart={setFromCart}
              />
            </Route>
            <Route path="/connexion">
              <Connexion fromCart={fromCart} />
            </Route>
            <Route path="/booking">
              <Booking save={SaveSeats} />
            </Route>
            <Route path="/private-room-booking">
              <PrivateRoomBooking save={SaveRooms} />
            </Route>
            <Route
              exact
              path="/user"
              render={(props) => {
                if (user) return <UserBoard {...props} />;
                return <Redirect to="/connexion" />;
              }}
            />
            <Route
              path="/user/profil"
              render={(props) => {
                if (user) return <Profil {...props} />;
                return <Redirect to="/connexion" />;
              }}
            />
            <Route
              path="/user/history"
              render={(props) => {
                if (user) return <History {...props} />;
                return <Redirect to="/connexion" />;
              }}
            />
            <Route
              path="/user/members"
              render={(props) => {
                if (user) return <Trombinoscope {...props} />;
                return <Redirect to="/connexion" />;
              }}
            />
            <Route
              exact
              path="/user"
              render={(props) => {
                if (user) return <UserBoard {...props} save={SaveProducts} />;
                return <Redirect to="/connexion" />;
              }}
            />
            <Route
              path="/user/profil"
              render={(props) => {
                if (user) return <Profil {...props} />;
                return <Redirect to="/connexion" />;
              }}
            />
            <Route
              path="/user/history"
              render={(props) => {
                if (user) return <History {...props} />;
                return <Redirect to="/connexion" />;
              }}
            />
            <Route
              path="/user/members"
              render={(props) => {
                if (user) return <Trombinoscope {...props} />;
                return <Redirect to="/connexion" />;
              }}
            />
          </Switch>
          <Footer />
        </Router>
      </AuthProvider>
    </ThemeProvider>
  );
}
