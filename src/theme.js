import { theme } from "@chakra-ui/core";

const customTheme = {
    ...theme,
    colors: {
        ...theme.colors,
        primary: "#1E4E59",
        secondary: "#D9BE3B",
        main: "#F2F2F0",
        beige: "#D9C39A",
        black: "#0D0D0D"
    },
    fonts: {
        body: "Roboto, sans-serif",
        heading: "Nature Spirit, serif"
    }
};

export default customTheme;
