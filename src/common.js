import axios from "axios";

export default axios.create({
    baseURL: "https://gustocoffee-staging.herokuapp.com/api/",
    headers: {
        "Content-type": "application/json"
    }
});
