const cache = {};
/**
 * Open the cache if it is available
 * https://developers.google.com/web/fundamentals/instant-and-offline/web-storage/cache-api#creating_and_opening_a_cache
 */
function set(key, data) {
  cache[key] = { data: data, cachedAt: new Date().getTime() };
}

function get(key) {
  return new Promise((resolve) => {
    resolve(
      cache[key] && cache[key].cachedAt + 15 * 60 * 1000 > new Date().getTime()
        ? cache[key].data
        : null
    );
  });
}
export default {
  set,
  get,
};
