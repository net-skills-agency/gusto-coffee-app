import axios from 'axios';
import React, {createContext, useContext, useState} from 'react';
import {useCookies} from 'react-cookie';
import {AuthStr} from "../components/constant/HomeConst";

// import { HandleError, NotifyMessage } from '../utils/HandleMessage';

const authContext = createContext();
// Provider qui crée l'objet d'auth et gère les states
const useProvideAuth = () => {

    const [cookie, setCookie, removeCookie] = useCookies(['user']);
    const [user, setUser] = useState(cookie.user);
    const today = new Date();
    const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);



    const getUserInfo = () => {
        axios.get('https://gustocoffee-staging.herokuapp.com/api/user',{ headers: { Authorization: AuthStr } }
        ).then(res => console.log('getUserInfo with response ', res));
    }


   // const  getUserInfo = async () => {
   //     const userInfo = await getUserInfoService();
   //     console.log('result from server', userInfo);
   //     return userInfo;
   // };




    const signin = (data) => {
        axios
            .post('https://gustocoffee-staging.herokuapp.com/api/login',
                data
            )
            .then(({res, data, status}) => {
                if (status === 200) {
                    setUser(data);
                    setCookie('user', data, {
                        expires: tomorrow,
                    });
                    console.log('result', res);
                    axios.defaults.headers["Authorization"] = "Bearer " + data.token;
                    localStorage.setItem('email', data.user.email);
                    localStorage.setItem('token', data.token);

                }
            })
            .catch(e => {
                console.log('Error lors de la connexion', e.response.data.error, e.response.status);
            });
    };

    const signup = (data) => {

        axios
            .post('https://gustocoffee-staging.herokuapp.com/api/register',
                data
            )
            .then(({data, status}) => {
                console.log('STATUS', status);
                if (status === 201) {
                    console.log('DATA', data);
                    setUser(data);
                    setCookie('user', data, {
                        expires: tomorrow,
                    });
                }

            })
            .catch(e => {
                console.log("Error lors de l'inscription", e.response.data.error, e.response.status);
            });

    };

    // const editMyProfile = (userData, navigation) => {
    //     editProfile(userData)
    //         .then(({ status, data }) => {
    //             if (status === 201) {
    //                 setUser(data.user);
    //                 setCookie('user', JSON.stringify(data.user), {
    //                     expires: tomorrow,
    //                 });
    //                 // ajout du token dans le header des requêtes http
    //                 axios.defaults.headers.authorization = `Baerer ${data.user.token}`;
    //                 // ajout de l'email en stockage local pour le garder en mémoire lors d'une déconnexion
    //                 localStorage.set('email', data.user.email);
    //                 localStorage.set('token', data.user.token);
    //                 addToast('Profil modifié avec succès !', {
    //                     appearance: 'success',
    //                     autoDismiss: true,
    //                 });
    //                //NotifyMessage('Profil modifié avec succès !');
    //                 navigation.goBack();
    //             }
    //         })
    //         .catch(e => {
    //             console.log('Error lors de la modification du profil', e.response.data.error, e.response.status);
    //         });
    // };

    // const editMyPassword = (passwords, navigation) => {
    //     editPassword(passwords)
    //         .then(({ status }) => {
    //             if (status === 201) {
    //                 addToast('Mot de passe modifié avec succès !', {
    //                     appearance: 'success',
    //                     autoDismiss: true,
    //                 });
    //                 //NotifyMessage('Mot de passe modifié avec succès !');
    //                 navigation.goBack();
    //             }
    //         })
    //         .catch(e => {
    //             console.log('Error lors de la modification du mot de passe', e.response.data.error, e.response.status);
    //         });
    // };

    // const removeMyAccount = () => {
    //     removeAccount()
    //         .then(({ status }) => {
    //             if (status === 200) {
    //                 // suppression du stockage local
    //                 const keys = localStorage.getAllKeys();
    //                 keys.forEach(key => localStorage.remove(key));
    //                 // suppression du cookie
    //                 removeCookie('user');
    //                 // mise à jour des valeurs
    //                 setUser(undefined);
    //                 axios.defaults.headers.authorization = undefined;
    //             }
    //         })
    //         .catch(e => {
    //             console.log('Error lors de la suppression du profil', e.response.data.error, e.response.status);
    //         });
    // };


    const signout = () => {
        removeCookie('user');
        setUser(undefined);
        localStorage.removeItem('token');
    };


    return {
        user,
        setUser,
        signin,
        signup,
        signout,
        getUserInfo,
        // editMyProfile,
        // editMyPassword,
        // removeMyAccount,

    };
};


export function AuthProvider({children}) {
    const auth = useProvideAuth();
    return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

export const useAuth = () => {
    return useContext(authContext);
};
export default useProvideAuth;
