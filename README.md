# Projet Gusto Coffee

Réalisation d'une application web pour réserver des espaces de coworking et de e-commerce.
Lien vers le site : https://gusto-coffee.netlify.app/

# Équipe

Net Skills Agency composée de David Boyaval, Bastien Domart, Fabien Labbé et Clément Langendorf.

# Gitlab

https://gitlab.com/net-skills-agency

Projet Back-end: https://gitlab.com/net-skills-agency/gusto_api <br />
Projet Front-end: https://gitlab.com/net-skills-agency/gusto_coffee-app

## Description des branches

Develop : branche de développement à partir de laquelle nous ferons les branches pour développer de nouvelles fonctionnalités.
Staging : branche pour le serveur de test.
Master : branche pour la mise en production.

## Prérequis

- serveur : Node.js
- langage : Javascript
- framework : Reactjs
- gestionnaire de package : Node Package Manager, Yarn

## Scripts

Dans le dossier du projet:

### `yarn install`

Lance l'installation des paquets de modules.

### `yarn start`

Lance l'application en mode développement.<br />
Ouvrir [http://localhost:3000](http://localhost:3000) dans le navigateur.

La page va se recharger après édition.<br />
Et les erreurs apparaîtront dans la console.

### `yarn test`

Lance l'utilitaire de tests.<br />
Voir la section [running tests](https://facebook.github.io/create-react-app/docs/running-tests) pour plus d'informations.

### `yarn build`

Construit l'application pour la production dans le dossier `build`.<br />

L'application est prête à être déployée!

Voir la section [deployment](https://facebook.github.io/create-react-app/docs/deployment) pour plus d'informations.
